jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 Products in the list
// * All 3 Products have at least one ItsSupplier

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/jp/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/jp/test/integration/pages/App",
	"com/jp/test/integration/pages/Browser",
	"com/jp/test/integration/pages/Master",
	"com/jp/test/integration/pages/Detail",
	"com/jp/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.jp.view."
	});

	sap.ui.require([
		"com/jp/test/integration/MasterJourney",
		"com/jp/test/integration/NavigationJourney",
		"com/jp/test/integration/NotFoundJourney",
		"com/jp/test/integration/BusyJourney",
		"com/jp/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});