jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/jp/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/jp/test/integration/pages/App",
	"com/jp/test/integration/pages/Browser",
	"com/jp/test/integration/pages/Master",
	"com/jp/test/integration/pages/Detail",
	"com/jp/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.jp.view."
	});

	sap.ui.require([
		"com/jp/test/integration/NavigationJourneyPhone",
		"com/jp/test/integration/NotFoundJourneyPhone",
		"com/jp/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});